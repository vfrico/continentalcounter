// *********************************************
//          Inicia la aplicación
// *********************************************
	if (!localStorage.getValue("partidas")) { //Mira si existe el registro de partidas en el navegador.
	//Si no existe, es la primera vez que se inicia la aplicación, 
	//por lo que tiene que iniciar el registro de partidas:
		localStorage["partidas"] = 0; // en este caso, con un 0, que significa que no existen partidas
		localStorage["partidaencurso"] = 0; //Además, indica que se está jugando la partida '0'
	}
	else if (localStorage["partidas"] == 0 || !(localStorage["partidas"])) //Si además no hay partidas o no existe el registro de partidas:
	{  	//Añade un mensaje para decir al usuario que no hay nada iniciado y que tiene que empezar una nueva
		$$('#encurso').append('<li data-icon="multiply"><strong>No hay partidas</strong><small>Empezar una nueva partida</small></li>');
	}
	if (!localStorage["partidaencurso"]) { //Si no existe el registro de la partida en curso, lo inicia
		localStorage["partidaencurso"] = 0;
	}
// **********************************************
// ******** Mostrar partidas en juego ***********
	for (var a = 0; a<parseInt(localStorage["partidas"]); a++) //Muestra las partidas guardadas en la memoria del navegador
	{
		var num = a+1;
		var nombre = "partidas."+num+".nombre";
		var jugadores = "partidas."+num+".jugadores";
		if(localStorage[nombre] && localStorage[jugadores]){ 
		//si existe en la memoria el nombre de la partida y el nombre de los usuarios, se continua para añadir el enlace y continuar la partida
			$$('#encurso').append('<li data-icon="users" id="INDEXpartidas"><!--<span class="icon users"></span>--><a href="#partida" onClick="setpartida('+num+');" data-router="article"><strong>'+localStorage[nombre]+'</strong><small>'+localStorage[jugadores]+'</small></a></li>'); //partidas encontradas
		}
	}	
// ****************************************
